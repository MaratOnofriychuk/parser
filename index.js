const ParserProduct = require('./parserProduct');
const fs = require('fs/promises');

(async () => {
  const productUrl = process.argv[2];
  const location = process.argv[3];

  validateArgv(productUrl, location);

  const initData = { productUrl, width: 1080, height: 1024 };
  const parser = await new ParserProduct(initData);

  await parser.openAllLocations();
  await parser.selectLocation(location);

  const data = await parser.getData();
  const bufferImage = await parser.getScreenshot();

  parser.close();

  await saveDataOfProduct('product.txt', data);
  await saveImage('screenshot.jpg', bufferImage);

  console.log('Готово!')
})().catch(error => console.log(error));

function validateArgv(productUrl, location) {
  if (!productUrl || !location) {
    let error = 'Не правильно запущен скрипт!\n';
    error += 'Пример:\n'
    error += 'node index.js https://www.vprok.ru/product/domik-v-derevne-dom-v-der-moloko-ster-3-2-950g--309202 "Санкт-Петербург и область"';
    
    throw new Error(error);
  }
}

async function saveDataOfProduct(file, data) {
  let str = Object.keys(data).reduce((p, key) => {
    return p + `${key}=${data[key]}\n`
  }, '');

  await fs.writeFile(file, str);
}

async function saveImage(file, data) {
  await fs.writeFile(file, data);
}
