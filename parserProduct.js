const puppeteer = require('puppeteer');

module.exports = class ParserProduct {
  constructor({ productUrl, width, height }) {
    return new Promise(async (resolve) => {
      const browser = await puppeteer.launch();
      const page = await browser.newPage();

      await page.setViewport({ width, height })
      await page.goto(productUrl);
      await page.waitForSelector('div.FirstHeader_region__lHCGj')

      this.browser = browser;
      this.page = page;

      resolve(this);
    })
  }

  async openAllLocations() {
    await this.page.click('div.FirstHeader_region__lHCGj');
  
    const interval = setInterval(() => {
      this.page.click('div.FirstHeader_region__lHCGj');
    }, 300);
  
    await this.page.waitForSelector('div.RegionModal_list__IzXxc', { timeout: 0 });
  
    clearImmediate(interval);
  }

  async selectLocation(location) {
    const locationButtons = await this.page.$x(`//div[contains(text(), '${location}')]`);
  
    if (!locationButtons.length) {
      throw new Error('Область указана не правильно!')
    }
  
    await locationButtons[0].click();

    await this.page.waitForSelector('div.FirstHeader_region__lHCGj', { timeout: 0 });
    await new Promise((res) => setTimeout(res, 1000));
  }

  async getData() {
    const data = {};

    data.price = (await this.#getTextBySelector('.Price_price__B1Q8E')).split(' ')[0];
    data.rating = (await this.#getTextBySelector('.Summary_title__Uie8u'));
    data.reviewCount = (await this.#getTextBySelector('.ActionsRow_reviews__k_SL5')).split(' ')[0];

    try {
      data.priceOld = (await this.#getTextBySelector('.Price_role_old__qW2bx')).split(' ')[0];
    } catch {}
    
    return data;
  }

  async getScreenshot() {
    return this.page.screenshot({
        fullPage: true,
      });
  }

  async close() {
    await this.browser.close();
  }

  async #getTextBySelector(selector) {
    const element = await this.page.waitForSelector(selector);
  
    return this.page.evaluate(element => element.textContent, element);
  }
}
